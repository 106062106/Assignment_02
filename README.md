# Assignment_02

1. Items Done
  a. Complete game process:
    Start Menu => Game Scene => OverScene => Start Menu
  b. Basic rules:
    A. Player can move, shoot, can be damaged by enemy bullets
    B. System can generate enemy and each enemy can move & attack.
    C. Map keep moving through the game.
  c. Jucify mechanisms:
    A. Level gets harder: level2 has boss
    B. Ultimate Skill: K key - shotgun
  d. Animations:
    A. Player when pressing W
  e. Particle Systems:
    A. Release Particle when bullet hit objects
  f. Sound effects:
    A. Two Sound Effect (Firing & Explosion)
  g. UI:
    A. Player Health
    B. Energy Bar
    D. Game Pause
  h. Appearance:
2. Bonus
  a. Boss:
  Unique movement & attack-mode
