var endState = {
  create: function() {
  game.add.image(0, 0, 'space');
  // Display the name of the game
  var nameLabel = game.add.text(game.width/2, 150, 'Game Over',
  {font:'50px Arial', fill:'#ffffff'});
  nameLabel.anchor.setTo(0.5, 0.5);
  // Show score
  var scoreLabel = game.add.text(game.width/2, game.height/2,
  'score: ' + game.global.score, {font:'25px Arial', fill:'#ffffff'});
  scoreLabel.anchor.setTo(0.5, 0.5);
  // Explain how to start the game
  var startLabel = game.add.text(game.width/2, game.height-100,
  'press the space key return to menu', {font:'25px Arial', fill:'#ffffff'});
  startLabel.anchor.setTo(0.5, 0.5);
  // Create a new Phaser keyboard variable: the up arrow key
  // When pressed, call the 'start'
  var spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  spaceKey.onDown.add(this.start, this);
  },
  start: function() {
    // Start the actual game
    game.state.start('menu');
  }
};