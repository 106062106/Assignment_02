var bootState = {
  preload: function () {
    game.load.image('progressBar', 'assets/img/progressBar.png');
  },
  create: function() {
    game.stage.backgroundColor = '#888888';
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.renderer.renderSession.roundPixels = true;
    // Start the load state.
    game.state.start('load');
  }
}; 