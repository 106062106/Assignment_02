// Global Variables
var curWave = 0;
var enemies = [];
var nextBlt = 0;
var nextSpr = 0;
var nextMsl = 0;
var waveEnd = false;
// Custom Data Type
Enemy = function(health, speed, fireRate, mov, pos_x, pos_y, type, bullet, level) {
  this.speed = speed;
  this.fireRate = fireRate;
  this.nextFire = game.time.now;
  this.mov = mov;
  this.type = type;
  this.bullet = bullet;

  this.sprite = game.add.sprite(pos_x, pos_y, type, 0, level.enemies);
  this.sprite.body.bounce.x = 1;
  this.sprite.anchor.setTo(0.5, 0.5);
  this.sprite.health = health;
  // Set velocity
  moveEnemy(this.sprite, this.mov, this.speed, level);
}
// Update Function
function updateHp(player, hpBar, audio, cur) {
  var frac = player.health/player.maxHealth;
  if (frac < 0) frac = 0;
  hpBar.width = frac*1000;
  if (frac == 0) {
    audio.play();
    game.time.events.add(3000, gameOver, cur);
  }
}
function updateEp(epBar) {
  if (epBar.width < 1000) epBar.width += 0.5;
  else epBar.width = 1000;
}
function updateScore(score) {
  score.text = 'score: ' + game.global.score;
}
function runEmitter(emitter, target, type) {
  emitter.x = target.x;
  emitter.y = target.y;
  switch(type) {
    case 'explode':
      emitter.start(true, 1000, null, 20);
    break;
  }
}
function pauseGame() {
  if (!game.paused) game.paused = true;
  else game.paused = false;
}
function checkEndStage(enemies, next) {
  if (waveEnd == true) {
    var enemy = enemies.getFirstExists(true);
    if (!enemy) {
      game.state.start(next);
    }
  }
}
function gameOver() {
  game.state.start('end');
}
// Move functions
function moveScene(tileSrite) {
	tileSrite.tilePosition.y += 5;
}
function movePlayer(player, downKey, upKey, leftKey, rightKey) {
	player.body.velocity.y = downKey.isDown * 100 - upKey.isDown * 100;
  player.body.velocity.x = rightKey.isDown * 100 - leftKey.isDown * 100;
  if (upKey.isDown) player.animations.play('forward');
  else {
    player.animations.stop();
    player.frame = 0;
  }
}
function moveEnemy(sprite, mov, speed, level) {
  switch (mov) {
    case 'dwn':
      sprite.body.velocity.x = 0;
      sprite.body.velocity.y = speed;
      break;
    case 'dr3':
      var rad = 30 / 180 * Math.PI;
      sprite.body.velocity.x = Math.sin(rad) * speed;
      sprite.body.velocity.y = Math.cos(rad) * speed;
      break;
    case 'dl3':
      var rad = 30 / 180 * Math.PI;
      sprite.body.velocity.x = -Math.sin(rad) * speed;
      sprite.body.velocity.y = Math.cos(rad) * speed;
      break;
    case 'rnd':
      var rad = game.rnd.integerInRange(-180, 180) / 180 * Math.PI;
      sprite.body.velocity.x = Math.sin(rad) * speed;
      sprite.body.velocity.y = Math.cos(rad) * speed;
      game.time.events.add(3500, moveEnemy, level, sprite, mov, speed, level);
      break;
  }
}
// Overlap Functions
function hitFloor(floor, enemy) {
  enemy.health = 0;
  enemy.kill();
}
function bltHitPlayer(player, object) {
  object.kill();
  player.health -= 2;
}
function sprHitPlayer(player, object) {
  object.kill();
  player.health -= 3;
}
function balHitPlayer(player, object) {
  object.kill();
  player.health -= 4;
}
function bltHitEnemy(bullet, enemy, audio) {
  bullet.kill();
  enemy.health -= 2;
  if (enemy.health < 1) {
    game.global.score += 5;
    audio.play();
    enemy.kill();
  }
}
function sprHitEnemy(spray, enemy, audio) {
  spray.kill();
  enemy.health -= 3;
  if (enemy.health < 1) {
    game.global.score += 5;
    audio.play();
    enemy.kill();
  }
}
function bumpEnemy(player, enemy) {
  player.health -= 10;
  enemy.health -= 10;
  if (enemy.health < 1) {
    game.global.score += 5;
    enemy.kill();
  }
}
// Enemy Generation Functions
function newWave(level) {
  var typeList = level.type[curWave];
  var numList = level.number[curWave];
  var posXList = level.pos_x[curWave];
  var posYList = level.pos_y[curWave];
  var atkList = level.attack[curWave];
  var movList = level.move[curWave];
  // Read enemy type
  for (var t = 0; t < typeList.length / 2; t++) {
    var type = typeList[2*t] + typeList[2*t+1];
    var health, speed;
    switch(type) {
      case 'fA':
        type = 'fighterA';
        health = 3;
        speed = 100;
        break;
      case 'fB':
        type = 'fighterB';
        health = 4;
        speed = 100;
        break;
      case 'GA':
        type = 'giantA';
        health = 30;
        speed = 100;
        break;
    }
    var bullet = atkList[2*t] + atkList[2*t+1];
    var fireRate;
    switch(bullet) {
      case 'br':
        bullet = "bullet_r";
        fireRate = 400;
        break;
      case 'sr':
        bullet = "spray_r";
        fireRate = 1200;
        break;
      case 'BA':
        bullet = "ball_r";
        fireRate = 600;
        break;
    }
    var mov = movList[3*t] + movList[3*t+1] + movList[3*t+2];
    var num = parseInt(numList[2*t] + numList[2*t+1]);
    for (var n = 0; n < num; n++) {
      var pos_x = parseInt(posXList[3*n] + posXList[3*n+1] + posXList[3*n+2]) * 10;
      var pos_y = parseInt(posYList[3*n] + posYList[3*n+1] + posYList[3*n+2]) * 10;
      if (pos_x < 0) pos_x += game.width;
      if (pos_y < 0) pos_y += game.height;
      var enemy = new Enemy(health, speed, fireRate, mov, pos_x, pos_y, type, bullet, level);
      if (mov == 'rnd') {
        enemy.sprite.body.collideWorldBounds = true;
        enemy.sprite.body.bounce.y = 1;
      }
      enemies.push(enemy);
    }
  }
}
// Fire Functions
function shoot(sprite, pos_x, pos_y, face, rad, speed) {
  sprite.reset(pos_x, pos_y);
  sprite.rotation = -face / 180 * Math.PI;
  sprite.body.velocity.x = Math.cos(rad) * speed;
  sprite.body.velocity.y = -Math.sin(rad) * speed;
}
function playerFire(player, epBar, fireKey, group, weapon, audio) {
	if (fireKey.isDown) {
    switch(weapon) {
      case 'bullet':
        if (game.time.now > nextBlt && group.countDead() > 0 && epBar.width > 15) {
          var rad = ((game.rnd.integerInRange(85, 95))/ 180) * Math.PI;
          var bullet = group.getFirstExists(false);
          epBar.width -= 15;
          nextBlt = game.time.now + 400;
          shoot(bullet, player.x, player.y, 90, rad, 400);
        }
        break;
      case 'spray':
        if (game.time.now > nextSpr && group.countDead() > 0 && epBar.width > 60) {
          epBar.width -= 60;
          nextSpr = game.time.now + 1200;
          for (var a = 0; a < 5; a++) {
            var rad = ((60+15*a)/180) * Math.PI;
            var spray = group.getFirstExists(false);
            shoot(spray, player.x, player.y, 90, rad, 400);
          }
        }
        break;
      case 'missle':
        if (game.time.now > nextMsl && group.countDead() > 0 && epBar.width > 100) {
          epBar.width -= 100;
          nextMsl = game.time.now + 2000;
          for (var a = 0; a < 2; a++) {
            var rad = ((45+90*a)/180) * Math.PI;
            var missle = group.getFirstExists(false);
            shoot(missle, player.x, player.y, 90, rad, 400);
          }
        }
        break;
    }
    audio.play();
  };
}
function enemyFire(level) {
  var weapon = enemies[level.i].bullet;
  if (game.time.now > enemies[level.i].nextFire) {
    switch(weapon) {
      case 'bullet_r':
        if (level.bullet_r.countDead() > 0) {
          var rad = -((game.rnd.integerInRange(85, 95))/ 180) * Math.PI;
          var bullet = level.bullet_r.getFirstExists(false);
          enemies[level.i].nextFire = game.time.now + 400;
          shoot(bullet, enemies[level.i].sprite.x, enemies[level.i].sprite.y, -90, rad, 400);
        }
        break;
      case 'spray_r':
        if (level.spray_r.countDead() > 5) {
          enemies[level.i].nextFire = game.time.now + 1200;
          for (var a = 0; a < 5; a++) {
            var rad = -((60+15*a)/180) * Math.PI;
            var spray = level.spray_r.getFirstExists(false);
            shoot(spray, enemies[level.i].sprite.x, enemies[level.i].sprite.y, -90, rad, 400);
          }
        }
        break;
      case 'ball_r':
        var mode = game.rnd.integerInRange(0, 1);
        switch(mode) {
          case 0:
            enemies[level.i].nextFire = game.time.now + 400;
            if (level.ball_r.countDead() > 0) {
              var difX = level.player.x - enemies[level.i].sprite.x;
              var difY = level.player.y - enemies[level.i].sprite.y;
              var dist = Math.sqrt(difX*difX + difY*difY);
              var ball = level.ball_r.getFirstExists(false);

              ball.reset(enemies[level.i].sprite.x, enemies[level.i].sprite.y);
              ball.rotation = 0;
              ball.body.velocity.x = (difX + game.rnd.integerInRange(-30, 30)) / dist * 400;
              ball.body.velocity.y = (difY + game.rnd.integerInRange(-30, 30)) / dist * 400;
            }
            break;
          case 1:
              enemies[level.i].nextFire = game.time.now + 1000;
            if (level.ball_r.countDead() > 8) {
              for (var r = 0; r < 8; r++) {
                var rad = (45*r/180) * Math.PI;
                var ball = level.ball_r.getFirstExists(false);
                shoot(ball, enemies[level.i].sprite.x, enemies[level.i].sprite.y, 0, rad, 400);
              }
            }
            break;
        }
    }
  }
}
// Setup Functions
function setup_group() {
  var group = game.add.group();
  group.enableBody = true;
  group.physicsBodyType = Phaser.Physics.ARCADE;
  return group;
}
function setup_walls(level) {
  game.add.sprite(-30, 0, 'wall', 0, level.walls);
  game.add.sprite(game.width, 0, 'wall', 0, level.walls);
  level.walls.setAll('body.immovable', true);
}
function setup_floor() {
  var floor = game.add.sprite(game.width / 2, game.height + 60, 'floor');
  game.physics.arcade.enable(floor);
  floor.anchor.setTo(0.5, 0);
  return floor;
}
function setup_Bar(pos_x, pos_y, sprite) {
  var bar = game.add.sprite(pos_x, pos_y, sprite);
  return bar;
}
function setup_emitter(num, type, spdXMin, spdXMax, spdYMin, spdYMax, maxSize) {
  var emitter = game.add.emitter(0, 0, num);
  emitter.makeParticles(type);
  emitter.setYSpeed(spdYMin, spdYMax);
  emitter.setXSpeed(spdXMin, spdXMax);
  emitter.setScale(maxSize, 0, maxSize, 0, 800);
  emitter.gravity = 0;
  return emitter;
}
function setup_player(pos_x, pos_y, sprite) {
  var player = game.add.sprite(pos_x, pos_y, sprite);
	game.physics.arcade.enable(player);
	player.anchor.setTo(0.5, 0.5);
  player.body.collideWorldBounds = true;
  // Animations
  player.animations.add('forward', [2, 3, 4], 12, true);

  player.maxHealth = 50;
  player.health = 50;
	return player;
}
function setup_bullets(maxNum, sprite) {
	var bullets = game.add.group();  
  bullets.enableBody = true;
  bullets.physicsBodyType = Phaser.Physics.ARCADE;
  bullets.createMultiple(maxNum, sprite, 0, false);
  bullets.setAll('anchor.x', 0.5);
  bullets.setAll('anchor.y', 0.5);
  bullets.setAll('outOfBoundsKill', true);
  bullets.setAll('checkWorldBounds', true);
	return bullets;
}
function setup_sound(type) {
  var audio = game.add.audio(type);
  return audio;
}