// Configuration
var game = new Phaser.Game(1120, 630, Phaser.AUTO, 'gameDiv');
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('end', endState);
game.state.add('level01', level01);
game.state.add('level02', level02);
game.state.start('boot');

game.global = {
  score: 0
}
