var loadState = {
  preload: function () {
  // Add a 'loading...' label on the screen
  var loadingLabel = game.add.text(game.width/2, 250, 'loading...', {font:'30px Arial', fill:'#ffffff'});
  loadingLabel.anchor.setTo(0.5, 0.5);
  // Display the progress bar
  var progressBar = game.add.sprite(game.width/2, 400, 'progressBar');
  progressBar.anchor.setTo(0.5, 0.5);
  game.load.setPreloadSprite(progressBar);
  // Load all game assets
  game.load.image('space', "assets/img/space.png");
  game.load.image('wall', "assets/img/wall.png");
  game.load.image('floor', "assets/img/floor.png");

  game.load.image('hpBar', "assets/img/hpBar.png");
  game.load.image('epBar', "assets/img/epBar.png");

  game.load.spritesheet('player', "assets/img/player/player_sheet.png", 61, 96);
  game.load.image('fighterA', "assets/img/enemies/fighterA.png");
  game.load.image('fighterB', "assets/img/enemies/fighterB.png");
  game.load.image('giantA', "assets/img/enemies/giantA.png");

  game.load.image('bullet_b', "assets/img/bullets/bullet_b.png");
  game.load.image('bullet_r', "assets/img/bullets/bullet_r.png");
  game.load.image('spray_b', "assets/img/bullets/spray_b.png");
  game.load.image('spray_r', "assets/img/bullets/spray_r.png");
  game.load.image('ball_r', "assets/img/bullets/ball_r.png");
  game.load.image('pixel_b', "assets/img/bullets/pixel_b.png");
  game.load.image('pixel_r', "assets/img/bullets/pixel_r.png");

  game.load.audio('shootGun', "assets/sound/bullet.wav");
  game.load.audio("explode", "assets/sound/explosion.wav");
  },
  create: function() {
  // Go to the menu state
  game.state.start('menu');
  }
};