var level01 = {
  preload: function() {
    // Reset Global Variables
    curWave = 0;
    enemies = [];
    nextBlt = 0;
    nextSpr = 0;
    waveEnd = false;
    game.global.score = 0;
  },
  create: function() {
    // Keyboard inputs
    this.upKey = game.input.keyboard.addKey(Phaser.KeyCode.W);
    this.leftKey = game.input.keyboard.addKey(Phaser.KeyCode.A);
    this.downKey = game.input.keyboard.addKey(Phaser.KeyCode.S);
		this.rightKey = game.input.keyboard.addKey(Phaser.KeyCode.D);
		
    this.bulletKey = game.input.keyboard.addKey(Phaser.KeyCode.J);
    this.sprayKey = game.input.keyboard.addKey(Phaser.KeyCode.K);
    this.missleKey = game.input.keyboard.addKey(Phaser.KeyCode.L);

    this.pauseKey = game.input.keyboard.addKey(Phaser.KeyCode.P);
    this.pauseKey.onDown.add(pauseGame, this);
    // Background
    this.space = game.add.tileSprite(0, 0, 1120, 630, 'space');
    // Score
    this.scoreLabel = game.add.text(30, 30, 'score: ' + game.global.score,
    {font:'18px Arial', fill:'#ffffff'});
    // Wall & Floor
    this.walls = setup_group();
    this.floor = setup_floor();
    setup_walls(level01);
    // Hp & Ep Bars
    this.hpBar = setup_Bar(game.width/2 - 500, game.height - 45, 'hpBar');
    this.epBar = setup_Bar(game.width/2 - 500, game.height - 25, 'epBar');
    this.epBar.width = 0;
    // Emitter
    this.blast_b = setup_emitter(150, 'pixel_b', -200, 200, -200, 200, 2);
    this.blast_r = setup_emitter(150, 'pixel_r', -200, 200, -200, 200, 2);
    // Player
    this.player = setup_player(game.width/2, game.height/2 + 200, 'player');
    // Enemies
    this.enemies = setup_group();
    game.time.events.add(4000, this.callWave, this);
		// Player Bullets
    this.bullet_b = setup_bullets(10, 'bullet_b');
    this.spray_b = setup_bullets(30, 'spray_b'); 
    // Enemy Bullets
    this.bullet_r = setup_bullets(30, 'bullet_r');
    this.spray_r = setup_bullets(80, 'spray_r');
    // Sounds
    this.gunSnd = setup_sound('shootGun');
    this.explodeSnd = setup_sound('explode');
  },
  update: function() {
    moveScene(this.space);
    movePlayer(this.player, this.downKey, this.upKey, this.leftKey, this.rightKey);
    updateHp(this.player, this.hpBar, this.explodeSnd, this);
    updateEp(this.epBar);
    updateScore(this.scoreLabel);
    playerFire(this.player, this.epBar, this.bulletKey, this.bullet_b, 'bullet', this.gunSnd);
    playerFire(this.player, this.epBar, this.sprayKey, this.spray_b, 'spray', this.gunSnd);
    for (this.i = 0; this.i < enemies.length; this.i++) {
      if (enemies[this.i].sprite.health > 0) enemyFire(level01);
    }
    game.physics.arcade.collide(this.enemies, this.walls, null, null, this);
    game.physics.arcade.collide(this.enemies, this.enemies, null, null, this);
    game.physics.arcade.overlap(this.bullet_r, this.player, this.bltHitPlayer, null, this);
    game.physics.arcade.overlap(this.spray_r, this.player, this.sprHitPlayer, null, this);
    game.physics.arcade.overlap(this.bullet_b, this.enemies, this.bltHitEnemy, null, this);
    game.physics.arcade.overlap(this.spray_b, this.enemies, this.sprHitEnemy, null, this);
    game.physics.arcade.overlap(this.player, this.enemies, bumpEnemy, null, this);
    game.physics.arcade.overlap(this.floor, this.enemies, hitFloor, null, this);
    checkEndStage(this.enemies, this.next);
  },
  callWave: function() {
    newWave(level01);
    var wait = parseInt(this.delay[curWave]);
    if (wait >= 0) game.time.events.add(wait*100, this.callWave, this);
    else waveEnd = true;
    curWave++;
  },
  callMute: function() {
    muteSound(level01);
  }
  ,
  bltHitPlayer: function(player, bullet) {
    runEmitter(this.blast_r, player, 'explode');
    bltHitEnemy(bullet, player);
  },
  sprHitPlayer: function(player, spray) {
    runEmitter(this.blast_r, player, 'explode');
    sprHitEnemy(spray, player);
  },
  bltHitEnemy: function(bullet, enemy) {
    runEmitter(this.blast_b, enemy, 'explode');
    bltHitEnemy(bullet, enemy, this.explodeSnd);
  },
  sprHitEnemy: function(spray, enemy) {
    runEmitter(this.blast_b, enemy, 'explode');
    sprHitEnemy(spray, enemy, this.explodeSnd);
  },
  // Wave Data
  type: ['fA', 'fA', 'fA', 'fB', 'fA',
         'fB', 'fB'],
  number: ['03', '02', '02', '02', '03',
           '01', '01'],
  pos_x: ['+20+56-20', '+10+30', '-10-30', '+20-20', '+20+56-20',
          '+40', '-40'],
  pos_y: ['+00+00+00', '+00+00', '+00+00', '+00+00', '+00+00+00',
          '+00', '+00'],
  attack: ['br', 'br', 'br', 'sr', 'br',
           'sr', 'sr'],
  move: ['dwn', 'dr3', 'dl3', 'dwn', 'dwn',
         'dl3', 'dr3'],
  delay: ['35', '0', '40', '35', '5',
          '0', '-1'],
  i: 0,
  next: 'level02'
};